﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace vafs.Views
{
    /// <summary>
    /// Interaktionslogik für MyGroups.xaml
    /// </summary>
    public partial class MyGroups : UserControl
    {
        public MyGroups()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var secondWindow = new AddGroupsDialog();
            secondWindow.Show();
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var secondWindow = new InviteMember();
            secondWindow.Show();
        }
    }
}
