﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using vafs.Model;

namespace vafs.ViewModel
{
    class MainViewModel : BaseViewModel
    {

        /// <summary>
		/// Festlegen des Startzustandes (Beachtet: Nach Login)
		/// </summary>
		private string activeMenuitem = "Calendar";
        private vafsDbContext db;

        public MainViewModel()
        {
            MenuitemCommandInitialisieren();
            db = new vafsDbContext();
            this.OnLogin = new RelayCommand<Window>(this.LoginCommand);
            this.OnRegister = new RelayCommand<Window>(this.RegisterCommand);
            this.LoginToRegister = new RelayCommand<Window>(this.LoginToRegisterCommand);
            this.RegisterToLogin = new RelayCommand<Window>(this.RegisterToLoginCommand);
            this.Logout = new RelayCommand<Window>(this.LogoutCommand);
            this.OnAddNotes = new RelayCommand<Window>(this.AddNotes);
            this.OnCreateGroup = new RelayCommand<Window>(this.CreateGroup);
            this.SaveChanges = new RelayCommand<Window>(this.ChangeProfile);
            this.OnAcceptInvitation = new RelayCommand<Window>(this.AcceptInvitation);
            this.OnDeclineInvitation = new RelayCommand<Window>(this.DeclineInvitation);
            this.OnSendInvitation = new RelayCommand<Window>(this.SendInvitation);
            OnJoinGroup = new RelayCommand(() => JoinGroup());
            this.OnLeaveGroup = new RelayCommand(() => LeaveGroup());
            this.OnDeleteNote = new RelayCommand(() => DeleteNote());
        }

        public ICommand SaveChanges { get; set; }


        public static u_user CurrentUser { get; private set; }


        #region Menuitem Properties
        /// <summary>
        /// Button command to switch between usercontrols 
        /// </summary>
        public ICommand ActivateMenuitem { get; private set; }
        /// <summary>
        /// Aktuell ausgewählter Menüpunkt.
        /// </summary>
        public string ActiveMenuitem
        {
            get => activeMenuitem;
            private set
            {
                if (activeMenuitem != value)
                {
                    activeMenuitem = value;
                    OnNotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// switch case with all possible usercontrols
        /// reads param of ActivateMenuitem(Button) and sets the ActiveMenuitem(View)
        /// </summary>
        private void MenuitemCommandInitialisieren()
        {
            ActivateMenuitem = new RelayCommand((param) =>
            {
                switch (param)
                {
                    case "Calendar":
                        ActiveMenuitem = "Calendar";
                        break;
                    case "Profile":
                        ActiveMenuitem = "Profile";
                        break;
                    case "Settings":
                        ActiveMenuitem = "Settings";
                        break;
                    case "MyGroups":
                        ActiveMenuitem = "MyGroups";
                        break;
                    case "PublicGroups":
                        ActiveMenuitem = "PublicGroups";
                        break;
                    case "MyNotes":
                        ActiveMenuitem = "MyNotes";
                        break;
                }
            });
        }

        #endregion

        #region Calendar


        /// <summary>
        /// returns the selected calendar information
        /// </summary>
        ///   

        private DateTime selectedCalendar = new DateTime(2020,01,01);

        public DateTime SelectedCalendar {
            get { return selectedCalendar; }
            set {
                selectedCalendar = value;
                OnNotifyPropertyChanged();
                OnNotifyPropertyChanged(nameof(NotesOfSelectedCalendar));
            }
        }

        public IEnumerable<n_note> NotesOfSelectedCalendar
        {
            get
            {
                if(CurrentUser != null && SelectedCalendar != null)
                {
                    var erg = from n in db.n_note
                    where n.n_deadline == SelectedCalendar
                    where n.n_u_user == CurrentUser.u_id
                    select n;
                    return erg.ToList();
                }
                else
                {
                    return new List<n_note>();
                }
            }
        }
        
        

        public IEnumerable<n_note> CalenderInformationList { //Liste von "Calendarinformation" (wir benützen ja notes statt calendarinformation): Notes beinhaltet Deadline, also nach Deadline ausgeben
            get {
                var erg = from n in db.n_note
                          where n.n_u_user == CurrentUser.u_id
                          select n;
                return erg;
            }
            private set { }
        }

        //Wir müssen kein ICommand für CreateCalendarInformation machen, weil wir ja nur mit Notes arbeiten. Unten bei "notes" gibt es den ICommand schon 

        #endregion

        #region Login

        /// <summary>
        /// Username Textbox
        /// </summary>
        public string UsernameLogin { get; set; }
        /// <summary>
        /// Password Textbox
        /// </summary>
        
        public string PasswordLogin { get; set; }
        /// <summary>
        /// Login Button Command
        /// </summary>
        public ICommand OnLogin { get; set; }
        /// <summary>
        /// create new user with Username, Password and Registerdate (now!)
        /// create new MainWindow (Menu window)
        /// switch current window from login to menu window
        /// close old window
        /// show new window
        /// </summary>
        /// <param name="window"> currently open window (Login window)</param>
        private void LoginCommand(Window window)
        {
            MainWindow main = new MainWindow();
            App.Current.MainWindow = main;

            if ((UsernameLogin == null) || (PasswordLogin == null))
            {
                //TODO
                //Error Felder dürfen nicht frei sein
                MessageBox.Show("Please enter username and password", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            var erg = from b in db.u_user
                      where b.u_username == UsernameLogin
                      select b;


            if ((erg.FirstOrDefault() != null) && erg.FirstOrDefault().u_password == ComputeSha256Hash(PasswordLogin))
            {
                CurrentUser = erg.FirstOrDefault();
                window.Close();
                main.Show();
            }
            else
            {
                //TODO
                //Fehler username stimmt nicht mit dem passwort überein
                MessageBox.Show("Username or password is wrong", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        #endregion

        #region Register

        public ICommand OnRegister { get; set; }
        public string UsernameRegister { get; set; }
        public string PasswordRegister { get; set; }
        public string EmailRegister { get; set; }


        private void RegisterCommand(Window window)
        {
            MainWindow main = new MainWindow();
            App.Current.MainWindow = main;

            var erg = from b in db.u_user
                      where b.u_username == UsernameRegister
                      select b;

            var erg2 = from b in db.u_user
                       where b.u_email == EmailRegister
                       select b;

            if ((erg.FirstOrDefault() == null) && (erg2.FirstOrDefault() == null))
            {
                
                u_user u = new u_user() { u_username = UsernameRegister, u_email = EmailRegister, u_password = ComputeSha256Hash(PasswordRegister), u_registerdate = DateTime.Now };
                CurrentUser = u;
                db.Entry(u).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                db.SaveChanges();
                window.Close();
                main.Show();
            }
            else
            {
                //TODO
                //Der Username oder Email existiert schon 

                MessageBox.Show("Username or Email already taken", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public ICommand LoginToRegister { get; set; }
        private void LoginToRegisterCommand(Window window)
        {
            Register register = new Register();
            App.Current.MainWindow = register;
            window.Close();
            register.Show();
        }

        public ICommand RegisterToLogin { get; set; }
        private void RegisterToLoginCommand(Window window)
        {
            Login login = new Login();
            App.Current.MainWindow = login;
            window.Close();
            login.Show();
        }
        #endregion

        #region Logout
        public ICommand Logout { get; set; }
        private void LogoutCommand(Window window)
        {
            if(window != null)
            {
                window.Close();
            }
        }
        #endregion

        #region AddNotes

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; } = DateTime.Now;
        public g_group Group { get; set; }

        public ICommand OnAddNotes { get; set; }

        public void AddNotes(Window window) {
            
            n_note n;

            if(Title == null || Description == null || Deadline == new DateTime())
            {
                MessageBox.Show("Please enter all the informations", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(Group == null)
            {
                n = new n_note() { n_createdate = DateTime.Now, n_deadline = Deadline, n_description = Description, n_title = Title, n_u_user = CurrentUser.u_id };
                db.Entry(n).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                db.SaveChanges();
                window.Close();
            }
            else
            {
                n = new n_note() { n_createdate = DateTime.Now, n_deadline = Deadline, n_description = Description, n_title = Title, n_u_user = CurrentUser.u_id, n_g_group = Group.g_id };
                db.Entry(n).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                db.SaveChanges();
                window.Close();
            }
            OnNotifyPropertyChanged(nameof(NotesList));
        }


        #endregion

        #region MyGroups

        public IEnumerable<n_note> No
        {
            get
            {
                if (SelectedGroup != null)
                {

                var erg = from k in db.n_note
                          where k.n_g_group == SelectedGroup.g_id
                          select k;
                return erg.ToList();
                }
                else
                {

                    return new List<n_note>();
                }
            }
        }

        public IEnumerable<u_user> UserOfSelectedGroup {
            get {
                if(selectedGroup != null)
                {
                    var erg = (from gm in db.u_g_groupmember
                              where gm.u_g_group == SelectedGroup.g_id
                              select gm.u_g_user).ToList();
                    var erg1 = (from u in db.u_user where erg.Any(x => x == u.u_id) select u).ToList();
                    return erg1;
                }
                else
                {
                    return new List<u_user>();
                }
                    
            }
        }

        private g_group selectedGroup;

        public g_group SelectedGroup {
            get =>  selectedGroup;
            set {
                selectedGroup = value;
                OnNotifyPropertyChanged();
                OnNotifyPropertyChanged(nameof(No)); //Funktioniert nicht wegen neuem Fenster
                OnNotifyPropertyChanged(nameof(UserOfSelectedGroup));
            }
        }
        
        public IEnumerable<c_calendarinformation> CalenderinformationOfSelectedGroup { get; set; } //Kalenderinformationen von der selektierten Gruppe

        public IEnumerable<g_group> MyPublicGroupsGroups //List von allen Public Gruppen eines Users
            { get
                {
                if (CurrentUser != null)
                {
                    var erg = from g in db.g_group
                              where g.u_g_groupmember.Any(x => x.u_g_user == CurrentUser.u_id)
                              where g.g_gs_groupstatus == 1
                              select g;
                    return erg.ToList();
                }
                return new List<g_group>();
                }
        }

        public IEnumerable<g_group> MyPrivateGroupsGroups { //Liste von allen Privaten Gruppen eines Users
            get {
                if (CurrentUser != null)
                {
                    var erg = from g in db.g_group
                              where g.u_g_groupmember.Any(x => x.u_g_user == CurrentUser.u_id)
                              where g.g_gs_groupstatus == 0
                              select g;
                    return erg.ToList();
                }
                return new List<g_group>();
            }
        }

        public IEnumerable<g_group> MyGroupsList
        { //Liste von allen Gruppen eines Users
            get
            {
                if (CurrentUser != null)
                {
                    var erg = from g in db.g_group
                              where g.u_g_groupmember.Any(x => x.u_g_user == CurrentUser.u_id)
                              select g;
                    return erg.ToList();
                }
                else
                {
                    return new List<g_group>();
                }
            }
        }

        //SEND INVITATIONS
        public ICommand OnSendInvitation { get; set; }
        public string InvitationUsername { get; set; }
        public DateTime InvitationExpireDate { get; set; }

        public void SendInvitation(Window window)
        {
            var user = (from us in db.u_g_groupmember
                        where us.u_g_group == SelectedGroup.g_id //GEHT NICHT WEIL HIER SELECTEDGROUP == NULL, mein verdacht: weil man in einem anderen fenster arbeitet, deswegen ist es null
                        where us.u_g_role == 2
                       select us.u_g_user).ToList();

            if (user.Any(x => x == CurrentUser.u_id))
            {
                if (InvitationUsername != null && InvitationExpireDate != null && (from g in db.u_g_groupmember where g.u_g_group == SelectedGroup.g_id select g.u_g_user).Count() < SelectedGroup.g_limit)
                {
                    i_invitation inv = new i_invitation { i_u_user = int.Parse((from u in db.u_user where u.u_username == InvitationUsername select u.u_id).ToString()), i_g_group = SelectedGroup.g_id, i_expiredate = InvitationExpireDate };
                    db.Entry(inv).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                    db.SaveChanges();
                }
                else
                {
                    MessageBox.Show("Username not found", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Keine Rechte", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            
        }

        public ICommand OnLeaveGroup { get; set; }

        public void LeaveGroup()
        {
            if(SelectedGroup == null)
            {
                MessageBox.Show("Keine Gruppe ausgewählt", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int anzahlUserInGruppe = (from gm in db.u_g_groupmember
                                      where gm.u_g_group == SelectedGroup.g_id
                                      select gm.u_g_user).Count();

            u_g_groupmember member = (from gm in db.u_g_groupmember
                                     where gm.u_g_group == SelectedGroup.g_id
                                     where gm.u_g_user == CurrentUser.u_id
                                     select gm).FirstOrDefault();
            if(anzahlUserInGruppe == 1)
            {
                db.Entry(SelectedGroup).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                db.SaveChanges();
            }

            db.Entry(member).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            db.SaveChanges();

            OnNotifyPropertyChanged(nameof(MyGroupsList));
            OnNotifyPropertyChanged(nameof(PublicGroupList));
            OnNotifyPropertyChanged(nameof(MyPublicGroupsGroups));
            OnNotifyPropertyChanged(nameof(MyPrivateGroupsGroups));
        }
        #endregion


        #region PublicGroups

        private g_group selectedPublicGroup;

        public g_group SelectedPublicGroup {
            get => selectedPublicGroup;
            set {
                selectedPublicGroup = value;
                OnNotifyPropertyChanged();
            }
        }

        public string GroupKey { get; set; }

        public IEnumerable<g_group> PublicGroupList {
            get {
                var allId = MyGroupsList.Select(x => x.g_id).ToList();
                var erg = from g in db.g_group
                          where !allId.Contains(g.g_id)
                          where g.g_gs_groupstatus == 1
                          select g;
                return erg.ToList();
            }
        }

        public ICommand OnJoinGroup { get; set; }
        public void JoinGroup()
        {
            if (SelectedPublicGroup != null)
            {
                u_g_groupmember ug = new u_g_groupmember { u_g_group = SelectedPublicGroup.g_id, u_g_role = 2, u_g_user = CurrentUser.u_id, u_g_membersince = DateTime.Now };
                db.Entry(ug).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                db.SaveChanges();
                
            }
            else if(GroupKey != null)
            {
                var erg = (from b in db.g_group
                           where b.g_key == GroupKey
                           select b).FirstOrDefault();
                u_g_groupmember ug = new u_g_groupmember { u_g_group = erg.g_id, u_g_role = 2, u_g_user = CurrentUser.u_id, u_g_membersince = DateTime.Now };
                db.Entry(ug).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                db.SaveChanges();
            }
            else
            {
                MessageBox.Show("Select a group", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            OnNotifyPropertyChanged(nameof(MyGroupsList));
            OnNotifyPropertyChanged(nameof(PublicGroupList));
            OnNotifyPropertyChanged(nameof(MyPublicGroupsGroups));
            OnNotifyPropertyChanged(nameof(MyPrivateGroupsGroups));
        }
        #endregion

        #region CreateGroup
        public ICommand OnCreateGroup { get; set; }
        public string GroupName { get; set; }
        public int Limit { get; set; }
        public string Key { get; set; }
        public string GroupStatus { get; set; }
        public IEnumerable<int> Limits
        {
            get
            {
                return new List<int> { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 40, 50, 60, 70, 80, 90, 100 };
            }
        }

        public List<string> AllGroupStatuses
        { //Liste von allen Gruppen eines Users
            get
            {
                var erg = from b in db.gs_groupstatus
                          select b.gs_bezeichnung;
                return erg.ToList();
            }
        }


        private void CreateGroup(Window window)
        {
            g_group g;
            u_g_groupmember ug;
            if (GroupName == null)
            {
                MessageBox.Show("Please enter all the informations", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int groupStatus;
            if(GroupStatus == "" || GroupStatus == null)
            {
                MessageBox.Show("Group status is empty", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if(GroupStatus == "private")
            {
                groupStatus = 0;
            }
            else
            {
                groupStatus = 1;
            }
            var erg1 = from b in db.g_group
                      where b.g_name == GroupName
                      select b;
            if(erg1.FirstOrDefault() != null)
            {
                MessageBox.Show("Name already taken", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(groupStatus == 0 && (Key == null || Key == ""))
            {
                //TODO
                //Wenn die Gruppe privat ist bracht man einen Key
                //Methode wir beendet
                MessageBox.Show("Please enter key", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                g = new g_group() { g_name = GroupName, g_key = Key, g_limit = Limit, g_gs_groupstatus = groupStatus };
                db.Entry(g).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                db.SaveChanges();

                ug = new u_g_groupmember() { u_g_group = g.g_id, u_g_membersince = DateTime.Now, u_g_role = 2, u_g_user = CurrentUser.u_id };
                db.Entry(ug).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                db.SaveChanges();
                window.Close();
                this.OnNotifyPropertyChanged(nameof(MyGroupsList));
                this.OnNotifyPropertyChanged(nameof(PublicGroupList));
                this.OnNotifyPropertyChanged(nameof(MyPublicGroupsGroups));
                this.OnNotifyPropertyChanged(nameof(MyPrivateGroupsGroups));
            }
            
        }

        #endregion

        #region MyProfile

        public ICommand OnAcceptInvitation;
        public ICommand OnDeclineInvitation;

        private void ChangeProfile(Window window)
        {
            //if(db.u_user.Any(x => x.u_username != newName))
            //{
            //    CurrentUser.u_username = newName;
            //    db.Entry(CurrentUser).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            //    db.SaveChanges();
            //}
            //else
            //{
            //    MessageBox.Show("Username already taken", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            db.Entry(CurrentUser).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        private i_invitation selectedInvitation;
        public i_invitation SelectedInvitation {
            get => selectedInvitation;
            set {
                selectedInvitation = value;
            }
        }
        public IEnumerable<i_invitation> Invitations {
            get {
                if (CurrentUser != null)
                {
                    var erg = from i in db.i_invitation
                              where i.i_u_user == CurrentUser.u_id
                              where i.i_expiredate >= DateTime.Now
                              select i;
                    return erg;
                }
                return new List<i_invitation>();
            }
            private set { }
        }

        public void AcceptInvitation(Window window)
        {
            u_g_groupmember gm = new u_g_groupmember { u_g_user = CurrentUser.u_id, u_g_group = SelectedInvitation.i_g_group, u_g_membersince = DateTime.Now, u_g_role = 2 };
            db.Entry(gm).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            db.SaveChanges();
            OnNotifyPropertyChanged("MyGroupsList");
            OnNotifyPropertyChanged("MyPrivateGroupsGroups");
            OnNotifyPropertyChanged("MyPublicGroupsGroups");
        }

        public void DeclineInvitation(Window window)
        {
            db.Entry(SelectedInvitation).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            db.SaveChanges();
            OnNotifyPropertyChanged();
        }

        #endregion

        #region ShowNotes

        private n_note selectedNote;
        public n_note SelectedNote
        {
            get => selectedNote;
            set
            {
                selectedNote = value;
                OnNotifyPropertyChanged();
            }
        }

        public IEnumerable<n_note> NotesList
        {
            get
            {
                if (CurrentUser != null)
                {
                    var erg = from b in db.n_note
                              where b.n_g_group == null && b.n_u_user == CurrentUser.u_id
                              select b;
                    return erg.ToList();
                }
                return new List<n_note>();
            }
        }

        public ICommand OnDeleteNote { get; set; }

        public void DeleteNote()
        {
            if (SelectedNote == null)
            {
                MessageBox.Show("Keine Notiz ausgewählt", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

                db.Entry(SelectedNote).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                db.SaveChanges();

            OnNotifyPropertyChanged(nameof(NotesList));
        }


        #endregion

    }
}