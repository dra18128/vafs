﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafs.Model
{
    public partial class c_calendarinformation
    {
        public c_calendarinformation()
        {
            n_note = new HashSet<n_note>();
        }

        [Key]
        [Column(TypeName = "int(11)")]
        public int c_id { get; set; }
        [Column(TypeName = "timestamp")]
        public DateTime c_createdate { get; set; }
        [Column(TypeName = "timestamp(1)")]
        public DateTime c_deadline { get; set; }
        [Column(TypeName = "int(11)")]
        public int c_u_user { get; set; }
        [Column(TypeName = "int(11)")]
        public int c_g_group { get; set; }

        [ForeignKey(nameof(c_g_group))]
        [InverseProperty(nameof(g_group.c_calendarinformation))]
        public virtual g_group c_g_groupNavigation { get; set; }
        [ForeignKey(nameof(c_u_user))]
        [InverseProperty(nameof(u_user.c_calendarinformation))]
        public virtual u_user c_u_userNavigation { get; set; }
        [InverseProperty("n_c_calendarinformationNavigation")]
        public virtual ICollection<n_note> n_note { get; set; }
    }
}
