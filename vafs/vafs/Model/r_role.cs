﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafs.Model
{
    public partial class r_role
    {
        public r_role()
        {
            u_g_groupmember = new HashSet<u_g_groupmember>();
        }

        [Key]
        [Column(TypeName = "int(11)")]
        public int r_id { get; set; }
        [Required]
        [Column(TypeName = "varchar(45)")]
        public string r_description { get; set; }

        [InverseProperty("u_g_roleNavigation")]
        public virtual ICollection<u_g_groupmember> u_g_groupmember { get; set; }
    }
}
