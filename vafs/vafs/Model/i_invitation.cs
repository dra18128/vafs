﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafs.Model
{
    public partial class i_invitation
    {
        [Key]
        [Column(TypeName = "int(11)")]
        public int i_u_user { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int i_g_group { get; set; }
        [Column(TypeName = "timestamp")]
        public DateTime i_expiredate { get; set; }

        [ForeignKey(nameof(i_g_group))]
        [InverseProperty(nameof(g_group.i_invitation))]
        public virtual g_group i_g_groupNavigation { get; set; }
        [ForeignKey(nameof(i_u_user))]
        [InverseProperty(nameof(u_user.i_invitation))]
        public virtual u_user i_u_userNavigation { get; set; }
    }
}
